const express = require('express')
const app = express()
const bodyParser = require('body-parser');

const request = require('request');
const parsePodcast = require('node-podcast-parser');
const crypto = require('crypto');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('port', 5000)

function checksum (str, algorithm, encoding) {
  return crypto
    .createHash(algorithm || 'md5')
    .update(str, 'utf8')
    .digest(encoding || 'hex')
}

const getFileChecksum = function(fileUrl){
  return new Promise((resolve, reject) => {

    console.log('promising: '+fileUrl)
    request.get(fileUrl, (error, res, data) => {
      console.log('requesting: '+fileUrl)
      if (error) {

        let err = new Error("Error fetching episode file");
        err.error = error;
        console.log('error fetching: '+fileUrl)
        console.error(error)
        reject(err);

      } else {
        if (res.statusCode == 200) {

          console.log('fetched: '+fileUrl)
          let hash = checksum(data)
          resolve(hash);

        } else {

          console.log('error fetching: '+fileUrl)
          let err = new Error("Unexpected status code: " + res.statusCode);
          err.res = res;
          reject(err);

        }
      }
    })
    .on('error', function(err) {
      console.log('error fetching: '+fileUrl)
      console.error(error)
    })

  })
}


app.get('/', (req, res) => {
  res.send("This server only accepts a POST call to /feed/read, parsing a feed URL for podcast episodes")
})

// Test with e.g. OSX Rested app and http://giantfraction.podbean.com/feed/
//
app.post('/feed/read', (req, res) => {
  let feedUrl = req.body.feed;

  request(feedUrl, (err, feedRes, feedXml) => {
    if (err) {
      console.error('Network error', err);
      return;
    }

    parsePodcast(feedXml, (err, feedJson) => {
      if (err) {
        console.error('Parsing error', err);
        return;
      }

      let promises = []
      feedJson['episodes'].map(function(e) {
        let promise = ''
        if (e['enclosure'] && e['enclosure']['url']) {
          promise = getFileChecksum(e['enclosure']['url'])
        }
        promises.push(promise)
      })

      Promise.all(promises).then((checksums) => {
        console.log('checksums:', checksums)
        checksums.map((checksum, i) => {
          feedJson['episodes'][i]['checksum'] = checksum
        })
        res.send(feedJson)
      })
      // console.log(feedJson);
    });
  });
})

app.listen(app.get('port'), function(){
  console.log('Started on port', app.get('port'))
})
